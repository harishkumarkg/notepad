package com.prem.premnotepad.datas.serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Entries {

    @SerializedName("sno")
    @Expose
    private String sno;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_name_image")
    @Expose
    private String companyNameImage;
    @SerializedName("company_address")
    @Expose
    private String companyAddress;
    @SerializedName("company_address_image")
    @Expose
    private String companyAddressImage;
    @SerializedName("vertical")
    @Expose
    private String vertical;
    @SerializedName("sub_vertical")
    @Expose
    private String subVertical;
    @SerializedName("contact_person_name")
    @Expose
    private String contactPersonName;
    @SerializedName("contact_person_image")
    @Expose
    private String contactPersonImage;
    @SerializedName("contact_no")
    @Expose
    private String contactNo;
    @SerializedName("contact_email_id")
    @Expose
    private String contactEmailId;
    @SerializedName("requirement")
    @Expose
    private String requirement;
    @SerializedName("existing_details")
    @Expose
    private String existingDetails;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("start_km")
    @Expose
    private String startKm;
    @SerializedName("end_km")
    @Expose
    private String endKm;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameImage() {
        return companyNameImage;
    }

    public void setCompanyNameImage(String companyNameImage) {
        this.companyNameImage = companyNameImage;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyAddressImage() {
        return companyAddressImage;
    }

    public void setCompanyAddressImage(String companyAddressImage) {
        this.companyAddressImage = companyAddressImage;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public String getSubVertical() {
        return subVertical;
    }

    public void setSubVertical(String subVertical) {
        this.subVertical = subVertical;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonImage() {
        return contactPersonImage;
    }

    public void setContactPersonImage(String contactPersonImage) {
        this.contactPersonImage = contactPersonImage;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getContactEmailId() {
        return contactEmailId;
    }

    public void setContactEmailId(String contactEmailId) {
        this.contactEmailId = contactEmailId;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getExistingDetails() {
        return existingDetails;
    }

    public void setExistingDetails(String existingDetails) {
        this.existingDetails = existingDetails;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStartKm() {
        return startKm;
    }

    public void setStartKm(String startKm) {
        this.startKm = startKm;
    }

    public String getEndKm() {
        return endKm;
    }

    public void setEndKm(String endKm) {
        this.endKm = endKm;
    }

}
