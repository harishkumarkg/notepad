package com.prem.premnotepad.ui.entries_list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.prem.premnotepad.R;
import com.prem.premnotepad.datas.database.DatabaseHandler;
import com.prem.premnotepad.datas.serializable.Entries;
import com.prem.premnotepad.engine.NotepadEngine;
import com.prem.premnotepad.ui.edit_entry.EditEntryActivity;

import java.util.List;

public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.MyViewHolder> {

    private Context context;
    private List<Entries> cartList;
    DatabaseHandler databaseHandler;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView companyName,serialNumber;
        ImageButton deleteEntry;
        public MyViewHolder(View view) {
            super(view);
            companyName = view.findViewById(R.id.company_name);
            serialNumber = view.findViewById(R.id.serial_number);
            deleteEntry = view.findViewById(R.id.delete_entry);
        }
    }

    public EntryAdapter(Context context, List<Entries> cartList) {
        this.context = context;
        this.cartList = cartList;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.entry_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Entries item = cartList.get(position);
        holder.companyName.setText(item.getCompanyName());
        holder.serialNumber.setText(item.getSno()+"");
        holder.companyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotepadEngine.myInstance.entries = item;
                context.startActivity(new Intent(context, EditEntryActivity.class));
            }
        });
        holder.deleteEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHandler.deleteEntry(item.getSno());
                context.startActivity(new Intent(context, EntriesListActivity.class));
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }
}