package com.prem.premnotepad.engine;

import com.prem.premnotepad.datas.serializable.Entries;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NotepadEngine {
    public Entries entries;
    public static NotepadEngine myInstance = new NotepadEngine();

    public String getSimpleCalenderDate(Calendar myCalendar) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return format.format(myCalendar.getTime());
    }
    public String getSimpleCalenderDateTime(Calendar myCalendar) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyyhh:mm:ss");
        return format.format(myCalendar.getTime());
    }

}
