package com.prem.premnotepad.ui.excel;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.prem.premnotepad.R;
import com.prem.premnotepad.datas.database.DatabaseHandler;
import com.prem.premnotepad.ui.base.BaseActivity;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

public class ExportActivity extends BaseActivity {

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        databaseHandler= new DatabaseHandler(this);
//        export();
        checkStoragePermission();
        export2();
    }

    private void export2() {
        Observable<Integer> observable = Observable.just(1);
        observable
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override public void onCompleted() {
                        Toast.makeText(ExportActivity.this, "Export Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override public void onError(Throwable e) {
                        Log.d("Test", "In onError()");
                    }

                    @Override public void onNext(Integer integer) {
                        try {
                            Workbook workbook = new XSSFWorkbook();
                            Sheet sheet = workbook.createSheet( "Sheet 1");
                            SQLiteDatabase sql_db = databaseHandler.getReadableDatabase();//here create a method ,and return SQLiteDatabaseObject.getReadableDatabase();
                            Cursor curCSV = sql_db.rawQuery("SELECT * FROM notepad",null);
                            String arrStr2[] = curCSV.getColumnNames();
                            Row row2 = sheet.createRow(0);
                            for(int j=0;j<arrStr2.length;j++){
                                Cell cell = row2.createCell(j);
                                cell.setCellValue(arrStr2[j]);
                            }
                            int i =1;
                            while(curCSV.moveToNext()){
                                //Which column you want to export you can add over here...
                                String arrStr[] ={curCSV.getString(0),curCSV.getString(1),
                                        curCSV.getString(2), curCSV.getString(3), curCSV.getString(4),
                                        curCSV.getString(5), curCSV.getString(6),
                                        curCSV.getString(7), curCSV.getString(8), curCSV.getString(9),
                                        curCSV.getString(10), curCSV.getString(11), curCSV.getString(12),
                                        curCSV.getString(13), curCSV.getString(14), curCSV.getString(15),
                                        curCSV.getString(16)};
                                Row row = sheet.createRow(i);

                                Cell cell = row.createCell(0);
                                cell.setCellValue(arrStr[0]);
                                cell = row.createCell(1);
                                cell.setCellValue(arrStr[1]);
                                cell = row.createCell(2);
                                cell.setCellValue(arrStr[2]);

                                File file;
                                InputStream inputStream;
                                CreationHelper helper;
                                Drawing drawing;
                                ClientAnchor anchor;
                                byte[] bytes;
                                int pictureIdx;
                                Picture pict;
                                try{
                                    file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                                            arrStr[3]+".jpg");
                                    inputStream = new FileInputStream(file);
                                    bytes = IOUtils.toByteArray(inputStream);
                                    pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                                    inputStream.close();
                                    helper = workbook.getCreationHelper();
                                    drawing = sheet.createDrawingPatriarch();
                                    anchor = helper.createClientAnchor();
                                    anchor.setCol1(3); //Column B
                                    anchor.setRow1(i); //Row 3
                                    anchor.setCol2(4); //Column C
                                    anchor.setRow2(i+1); //Row 4
                                    pict = drawing.createPicture(anchor, pictureIdx);
                                }catch (Exception e){
                                    cell = row.createCell(3);
                                    cell.setCellValue(arrStr[3]);
                                }



                                cell = row.createCell(4);
                                cell.setCellValue(arrStr[4]);

                                try{
                                    file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                                            arrStr[5]+".jpg");
                                    inputStream = new FileInputStream(file);
                                    bytes = IOUtils.toByteArray(inputStream);
                                    pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                                    inputStream.close();
                                    helper = workbook.getCreationHelper();
                                    drawing = sheet.createDrawingPatriarch();
                                    anchor = helper.createClientAnchor();
                                    anchor.setCol1(5); //Column B
                                    anchor.setRow1(i); //Row 3
                                    anchor.setCol2(6); //Column C
                                    anchor.setRow2(i+1); //Row 4
                                    pict = drawing.createPicture(anchor, pictureIdx);

                                }catch (Exception e){
                                    cell = row.createCell(5);
                                    cell.setCellValue(arrStr[5]);
                                }

                                cell = row.createCell(6);
                                cell.setCellValue(arrStr[6]);
                                cell = row.createCell(7);
                                cell.setCellValue(arrStr[7]);
                                cell = row.createCell(8);
                                cell.setCellValue(arrStr[8]);

                                try{
                                    file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                                            arrStr[9]+".jpg");
                                    inputStream = new FileInputStream(file);
                                    bytes = IOUtils.toByteArray(inputStream);
                                    pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                                    inputStream.close();
                                    helper = workbook.getCreationHelper();
                                    drawing = sheet.createDrawingPatriarch();
                                    anchor = helper.createClientAnchor();
                                    anchor.setCol1(9); //Column B
                                    anchor.setRow1(i); //Row 3
                                    anchor.setCol2(10); //Column C
                                    anchor.setRow2(i+1); //Row 4
                                    pict = drawing.createPicture(anchor, pictureIdx);
                                }catch (Exception e){
                                    cell = row.createCell(9);
                                    cell.setCellValue(arrStr[9]);
                                }

                                cell = row.createCell(10);
                                cell.setCellValue(arrStr[10]);
                                cell = row.createCell(11);
                                cell.setCellValue(arrStr[11]);
                                cell = row.createCell(12);
                                cell.setCellValue(arrStr[12]);
                                cell = row.createCell(13);
                                cell.setCellValue(arrStr[13]);
                                cell = row.createCell(14);
                                cell.setCellValue(arrStr[14]);
                                cell = row.createCell(15);
                                cell.setCellValue(arrStr[15]);
                                cell = row.createCell(16);
                                cell.setCellValue(arrStr[16]);

                /*for(int j=0;j<arrStr.length;j++){
                    XSSFCell cell = row.createCell(j);
                    cell.setCellValue(arrStr[j]);
                }*/
                                i++;
                            }

                            File exportDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");
                            if (!exportDir.exists()){
                                exportDir.mkdirs();
                            }
                            //Save excel to HDD Drive
                            File pathToFile = new File(exportDir, "csvfilename.xlsx");
                            if (!pathToFile.exists()) {
                                pathToFile.createNewFile();
                            }
                            FileOutputStream fos = new FileOutputStream(pathToFile);
                            workbook.write(fos);
                            fos.close();
                            Uri bmpUri= Uri.fromFile(pathToFile);

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(bmpUri, "application/vnd.ms-excel");
                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Log.d("Excel", "ActivityNotFoundException : ", e);
                            }
                            System.out.println("Done");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }}
                });
    }

    private static XSSFSheet createSheet(XSSFWorkbook wb, String prefix, boolean isHidden) {
        XSSFSheet sheet = null;
        int count = 0;

        for (int i = 0; i < wb.getNumberOfSheets(); i++) {
            String sName = wb.getSheetName(i);
            if (sName.startsWith(prefix))
                count++;
        }

        if (count > 0) {
            sheet = wb.createSheet(prefix + count);
        } else
            sheet = wb.createSheet(prefix);

        if (isHidden)
            wb.setSheetHidden(wb.getNumberOfSheets() - 1, XSSFWorkbook.SHEET_STATE_VERY_HIDDEN);

        return sheet;
    }


    private void checkStoragePermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            export2();
                            // do you work now
                        }else{
                            checkStoragePermission();

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }



    private void export( ) {
        Observable<Integer> observable = Observable.just(1);
        observable
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override public void onCompleted() {
                        Toast.makeText(ExportActivity.this, "Export Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override public void onError(Throwable e) {
                        Log.d("Test", "In onError()");
                    }

                    @Override public void onNext(Integer integer) {
                        File exportDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");
                        if (!exportDir.exists()){
                            exportDir.mkdirs();
                        }
                        File file = new File(exportDir, "csvfilename.csv");
                        try{
                            file.createNewFile();
                            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                            SQLiteDatabase sql_db = databaseHandler.getReadableDatabase();//here create a method ,and return SQLiteDatabaseObject.getReadableDatabase();
                            Cursor curCSV = sql_db.rawQuery("SELECT * FROM notepad",null);
                            csvWrite.writeNext(curCSV.getColumnNames());
                            while(curCSV.moveToNext()){
                                //Which column you want to export you can add over here...
                                String arrStr[] ={curCSV.getString(0),curCSV.getString(1),
                                        curCSV.getString(2), curCSV.getString(3), curCSV.getString(4),
                                        curCSV.getString(5), curCSV.getString(6),
                                        curCSV.getString(7), curCSV.getString(8), curCSV.getString(9),
                                        curCSV.getString(10), curCSV.getString(11), curCSV.getString(12),
                                        curCSV.getString(13), curCSV.getString(14), curCSV.getString(15),
                                        curCSV.getString(16)};
                                csvWrite.writeNext(arrStr);
                            }
                            csvWrite.close();
                            curCSV.close();
                        }
                        catch(Exception sqlEx){
                            Log.e("Error:", sqlEx.getMessage(), sqlEx);
                        }
                    }
                });
    }
}
