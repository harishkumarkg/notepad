package com.prem.premnotepad.ui;

import android.os.Bundle;

import com.prem.premnotepad.R;
import com.prem.premnotepad.ui.base.BaseActivity;

public class ImageViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
    }
}
