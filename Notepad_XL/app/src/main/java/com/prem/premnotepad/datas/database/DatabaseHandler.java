package com.prem.premnotepad.datas.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.prem.premnotepad.datas.serializable.Entries;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Comp11 on 1/8/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "muktha";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS `notepad` (`sno` INTEGER PRIMARY KEY AUTOINCREMENT ," +
                "`date` varchar(45) NOT NULL, `company_name` varchar(150) NOT NULL, " +
                " `company_name_image` varchar(150) NOT NULL, `company_address` varchar(150) NOT NULL" +
                ", `company_address_image` varchar(150) NOT NULL, `vertical` varchar(150) NOT NULL" +
                ", `sub_vertical` varchar(150) NOT NULL, `contact_person_name` varchar(150) NOT NULL," +
                " `contact_person_image` varchar(150) NOT NULL" +
                ", `contact_no` varchar(150) NOT NULL, `contact_email_id` varchar(150) NOT NULL" +
                ", `requirement` varchar(150) NOT NULL, `existing_details` varchar(150) NOT NULL" +
                ", `remarks` varchar(150) NOT NULL, `start_km` varchar(150) NOT NULL" +
                ", `end_km` varchar(150) NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void editEntry(Entries entries) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("date", entries.getDate());
        values.put("company_name", entries.getCompanyName());
        values.put("company_name_image", entries.getCompanyNameImage());
        values.put("company_address", entries.getCompanyAddress());
        values.put("company_address_image", entries.getCompanyAddressImage());
        values.put("vertical", entries.getVertical());
        values.put("sub_vertical", entries.getSubVertical());
        values.put("contact_person_name", entries.getContactPersonName());
        values.put("contact_person_image", entries.getContactPersonImage());
        values.put("contact_no", entries.getContactNo());
        values.put("contact_email_id", entries.getContactEmailId());
        values.put("requirement", entries.getRequirement());
        values.put("existing_details", entries.getExistingDetails());
        values.put("remarks", entries.getRemarks());
        values.put("start_km", entries.getStartKm());
        values.put("end_km", entries.getEndKm());
        db.update("notepad", values, "sno = ?",new String[] { entries.getSno() });
        db.close(); // Closing database connection
    }

    public void deleteEntry(String sno) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("notepad", "sno = ?",
                new String[] { String.valueOf(sno) });
        db.close();
    }

    public void addEntry(Entries entries) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("date", entries.getDate());
        values.put("company_name", entries.getCompanyName());
        values.put("company_name_image", entries.getCompanyNameImage());
        values.put("company_address", entries.getCompanyAddress());
        values.put("company_address_image", entries.getCompanyAddressImage());
        values.put("vertical", entries.getVertical());
        values.put("sub_vertical", entries.getSubVertical());
        values.put("contact_person_name", entries.getContactPersonName());
        values.put("contact_person_image", entries.getContactPersonImage());
        values.put("contact_no", entries.getContactNo());
        values.put("contact_email_id", entries.getContactEmailId());
        values.put("requirement", entries.getRequirement());
        values.put("existing_details", entries.getExistingDetails());
        values.put("remarks", entries.getRemarks());
        values.put("start_km", entries.getStartKm());
        values.put("end_km", entries.getEndKm());
        db.insert("notepad", null, values);
        db.close(); // Closing database connection
    }


    public List<Entries> getEntries() {
        List<Entries> townBeatList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM notepad ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Entries entries = new Entries();
                entries.setSno(cursor.getString(0));
                entries.setDate(cursor.getString(1));
                entries.setCompanyName(cursor.getString(2));
                entries.setCompanyNameImage(cursor.getString(3));
                entries.setCompanyAddress(cursor.getString(4));
                entries.setCompanyAddressImage(cursor.getString(5));
                entries.setVertical(cursor.getString(6));
                entries.setSubVertical(cursor.getString(7));
                entries.setContactPersonName(cursor.getString(8));
                entries.setContactPersonImage(cursor.getString(9));
                entries.setContactNo(cursor.getString(10));
                entries.setContactEmailId(cursor.getString(11));
                entries.setRequirement(cursor.getString(12));
                entries.setExistingDetails(cursor.getString(13));
                entries.setRemarks(cursor.getString(14));
                entries.setStartKm(cursor.getString(15));
                entries.setEndKm(cursor.getString(16));
                townBeatList.add(entries);
            } while (cursor.moveToNext());
        }
        return townBeatList;
    }
}