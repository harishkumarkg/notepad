package com.prem.premnotepad.ui.base;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prem.premnotepad.R;
import com.prem.premnotepad.datas.serializable.MenuItemsList;
import com.prem.premnotepad.ui.entries_list.EntriesListActivity;
import com.prem.premnotepad.ui.excel.ExportActivity;
import com.prem.premnotepad.ui.notepad.NotepadActivity;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener {

    private FrameLayout view_stub; //This is the framelayout to keep your content view
    private NavigationView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;
    RecyclerView customMenuNavigation;
    MenuListAdapter menuItemListAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    TextView username;
    List<MenuItemsList> menuItemsList;
    List<MenuItemsList> subMenuItemsList;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void viswa(){
        Toast.makeText(this, "Hi", Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {

        mDrawerLayout.closeDrawers();
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.drawer_layout);// The base layout that contains your navigation drawer.
        view_stub = findViewById(R.id.view_stub);
        navigation_view = findViewById(R.id.navigation_view);
        username = findViewById(R.id.username);
        customMenuNavigation = findViewById(R.id.custom_menu_navigation);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                0, 0);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerMenu = navigation_view.getMenu();
        for(int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }
        navigation_view.setItemIconTintList(null);
        getMenuItems();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        username.setText("Hi!");
    }

    private void getMenuItems() {
        menuItemsList = new ArrayList<>();
        menuItemsList.add(setMenuItem(NotepadActivity.class,"New Entry",R.drawable.ic_add_box,null));
        menuItemsList.add(setMenuItem(EntriesListActivity.class,"List All Entries",R.drawable.ic_playlist_add_check,null));
        menuItemsList.add(setMenuItem(ExportActivity.class,"Excel Export",R.drawable.ic_import_export,null));
        setMenuRecycler();
    }

    private void setMenuRecycler() {
        menuItemListAdapter = new MenuListAdapter(this, menuItemsList);
        customMenuNavigation.setAdapter(menuItemListAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        customMenuNavigation.setLayoutManager(mLayoutManager);
        customMenuNavigation.setItemAnimator(new DefaultItemAnimator());
    }

    private MenuItemsList setMenuItem(Class<?> className, String menuName, int iconPath, List<MenuItemsList> subMenuItems) {
        MenuItemsList menuItemsList = new MenuItemsList();
        menuItemsList.setaClass(className);
        menuItemsList.setIconID(iconPath);
        menuItemsList.setMenuName(menuName);
        menuItemsList.setSubMenuItemsList(subMenuItems);
        return menuItemsList;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}