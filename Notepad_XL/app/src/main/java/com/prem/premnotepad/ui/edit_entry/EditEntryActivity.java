package com.prem.premnotepad.ui.edit_entry;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.prem.premnotepad.R;
import com.prem.premnotepad.datas.database.DatabaseHandler;
import com.prem.premnotepad.datas.serializable.Entries;
import com.prem.premnotepad.engine.NotepadEngine;
import com.prem.premnotepad.ui.base.BaseActivity;
import com.prem.premnotepad.ui.entries_list.EntriesListActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditEntryActivity extends BaseActivity {

    @BindView(R.id.serial_number)
    TextView serialNumber;
    @BindView(R.id.date_selected)
    EditText date_selected;
    @BindView(R.id.company_name)
    EditText company_name;
    @BindView(R.id.company_address)
    EditText company_address;
    @BindView(R.id.contact_number)
    EditText contact_number;
    @BindView(R.id.vertical)
    EditText vertical;
    @BindView(R.id.sub_vertical)
    EditText sub_vertical;
    @BindView(R.id.contact_person)
    EditText contact_person;
    @BindView(R.id.contact_email_id)
    EditText contact_email_id;
    @BindView(R.id.requirement)
    EditText requirement;
    @BindView(R.id.existing_details)
    EditText existing_details;
    @BindView(R.id.remarks)
    EditText remarks;
    @BindView(R.id.start_km)
    EditText start_km;
    @BindView(R.id.end_km)
    EditText end_km;
    @BindView(R.id.company_name_image)
    ImageView company_name_image;
    @BindView(R.id.company_address_image)
    ImageView company_address_image;
    @BindView(R.id.contact_person_image)
    ImageView contact_person_image;
    final static int CAMERA_COMPANYNAME = 120,CAMERA_COMPANYADDRESS = 121,CAMERA_PERSONNAME = 122;
    File photo;
    Intent cameraIntent;
    DatabaseHandler databaseHandler;
    String companyImageName="",companyAddress="",companyPerson="save_entry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_edit_entry);
        getSupportActionBar().setTitle("Edit Entry");
        ButterKnife.bind(this);
        databaseHandler = new DatabaseHandler(this);
        getPermission();
        setItem();
    }

    private void setItem() {
        Entries entries = new Entries();
        entries = NotepadEngine.myInstance.entries;
        serialNumber.setText(entries.getSno());
        date_selected.setText(entries.getDate());
        company_name.setText(entries.getCompanyName());
        companyImageName = entries.getCompanyNameImage();
        File file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                companyImageName+".jpg");
        if(file.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            company_name_image.setImageBitmap(myBitmap);
        }
        company_address.setText(entries.getCompanyAddress());
        companyAddress = entries.getCompanyAddressImage();
        File file2 = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                companyAddress+".jpg");
        if(file2.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(file2.getAbsolutePath());
            company_address_image.setImageBitmap(myBitmap);
        }
        vertical.setText(entries.getVertical());
        sub_vertical.setText(entries.getSubVertical());
        contact_person.setText(entries.getContactPersonName());
        companyPerson = entries.getContactPersonImage();
        File file3 = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                companyPerson+".jpg");
        if(file3.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(file3.getAbsolutePath());
            contact_person_image.setImageBitmap(myBitmap);
        }
        contact_number.setText(entries.getContactNo());
        contact_email_id.setText(entries.getContactEmailId());
        requirement.setText(entries.getRequirement());
        existing_details.setText(entries.getExistingDetails());
        remarks.setText(entries.getRemarks());
        start_km.setText(entries.getStartKm());
        end_km.setText(entries.getEndKm());
    }

    private void getPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @OnClick(R.id.save_entry)
    public void saveEntry() {
        Entries entries = new Entries();
        entries.setSno(serialNumber.getText().toString());
        entries.setDate(date_selected.getText().toString());
        entries.setCompanyName(company_name.getText().toString());
        entries.setCompanyNameImage(companyImageName);
        entries.setCompanyAddress(company_address.getText().toString());
        entries.setCompanyAddressImage(companyAddress);
        entries.setVertical(vertical.getText().toString());
        entries.setSubVertical(sub_vertical.getText().toString());
        entries.setContactPersonName(contact_person.getText().toString());
        entries.setContactPersonImage(companyPerson);
        entries.setContactNo(contact_number.getText().toString());
        entries.setContactEmailId(contact_email_id.getText().toString());
        entries.setRequirement(requirement.getText().toString());
        entries.setExistingDetails(existing_details.getText().toString());
        entries.setRemarks(remarks.getText().toString());
        entries.setStartKm(start_km.getText().toString());
        entries.setEndKm(end_km.getText().toString());
        databaseHandler.editEntry(entries);
        openEntriesListActivity();
    }

    private void openEntriesListActivity() {
        startActivity(new Intent(EditEntryActivity.this, EntriesListActivity.class));
        finish();
    }

    @OnClick(R.id.company_name_image)
    public void saveTheImage() {
        companyImageName = NotepadEngine.myInstance.getSimpleCalenderDateTime(Calendar.getInstance())+"companyImage";
        cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_COMPANYNAME);
    }

    @OnClick(R.id.company_address_image)
    public void saveAddressImage() {
        companyAddress = NotepadEngine.myInstance.getSimpleCalenderDateTime(Calendar.getInstance())+"companyAddress";
        cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_COMPANYADDRESS);
    }

    @OnClick(R.id.contact_person_image)
    public void savePersonImage() {
        companyPerson = NotepadEngine.myInstance.getSimpleCalenderDateTime(Calendar.getInstance())+"companyPerson";
        cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_PERSONNAME);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File photo = new File(Environment.getExternalStorageDirectory()+"/premfolder");
        photo.mkdirs();
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap thumbnail;
        ByteArrayOutputStream bytes;
        File file;
        switch (requestCode) {
            case CAMERA_COMPANYNAME:
                thumbnail = (Bitmap) data.getExtras().get("data");
                company_name_image.setImageBitmap(thumbnail);
                bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                        companyImageName+".jpg");
                try {
                    file.createNewFile();
                    FileOutputStream fo = new FileOutputStream(file);
                    //5
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case CAMERA_COMPANYADDRESS:
                thumbnail = (Bitmap) data.getExtras().get("data");
                company_address_image.setImageBitmap(thumbnail);
                bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                        companyAddress+".jpg");
                try {
                    file.createNewFile();
                    FileOutputStream fo = new FileOutputStream(file);
                    //5
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case CAMERA_PERSONNAME:
                thumbnail = (Bitmap) data.getExtras().get("data");
                contact_person_image.setImageBitmap(thumbnail);
                bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                file = new File(Environment.getExternalStorageDirectory()+"/premfolder"+File.separator +
                        companyPerson+".jpg");
                try {
                    file.createNewFile();
                    FileOutputStream fo = new FileOutputStream(file);
                    //5
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
        }

    }


}
