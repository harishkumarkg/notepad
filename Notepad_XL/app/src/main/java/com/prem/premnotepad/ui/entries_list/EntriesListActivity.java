package com.prem.premnotepad.ui.entries_list;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.prem.premnotepad.R;
import com.prem.premnotepad.datas.database.DatabaseHandler;
import com.prem.premnotepad.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EntriesListActivity extends BaseActivity {

    @BindView(R.id.items_data_list)
    RecyclerView itemDataList;
    LinearLayoutManager mLayoutManager;
    EntryAdapter entryAdapter;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entries_list);
        ButterKnife.bind(this);
        databaseHandler = new DatabaseHandler(this);
        setItemRecyclerView();
    }

    private void setItemRecyclerView() {
        entryAdapter = new EntryAdapter(this, databaseHandler.getEntries());
        mLayoutManager = new LinearLayoutManager(this);
        itemDataList.setAdapter(entryAdapter);
        itemDataList.setLayoutManager(mLayoutManager);
    }
}
