package com.prem.premnotepad;

import android.os.Bundle;

import com.prem.premnotepad.datas.database.DatabaseHandler;
import com.prem.premnotepad.ui.base.BaseActivity;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity  {

    DatabaseHandler databaseHandler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        databaseHandler= new DatabaseHandler(this);
    }

    /*@OnClick(R.id.entries_list)
    public void openEntriesList(){
        startActivity(new Intent(MainActivity.this, EntriesListActivity.class));
    }*/

    /*@OnClick(R.id.export_data)
    public void exportData(){
        File exportDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");

        if (!exportDir.exists())
        {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, "csvfilename.csv");

        try
        {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            SQLiteDatabase sql_db = databaseHandler.getReadableDatabase();//here create a method ,and return SQLiteDatabaseObject.getReadableDatabase();
            Cursor curCSV = sql_db.rawQuery("SELECT * FROM notepad",null);
            csvWrite.writeNext(curCSV.getColumnNames());

            while(curCSV.moveToNext())
            {
                //Which column you want to export you can add over here...
                String arrStr[] ={curCSV.getString(0),curCSV.getString(1),
                        curCSV.getString(2), curCSV.getString(3), curCSV.getString(4),
                        curCSV.getString(5), curCSV.getString(6),
                        curCSV.getString(7), curCSV.getString(8), curCSV.getString(9),
                        curCSV.getString(10), curCSV.getString(11), curCSV.getString(12),
                        curCSV.getString(13), curCSV.getString(14), curCSV.getString(15),
                        curCSV.getString(16)};
                csvWrite.writeNext(arrStr);
            }

            csvWrite.close();
            curCSV.close();
        }
        catch(Exception sqlEx)
        {
            Log.e("Error:", sqlEx.getMessage(), sqlEx);
        }
    }*/

    /*@OnClick(R.id.open_notepead)
    public void setOpenNotepead(){
        startActivity(new Intent(MainActivity.this, NotepadActivity.class));
    }*/
}
